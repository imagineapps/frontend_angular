import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from "./material/material.module";
import {MatNativeDateModule} from '@angular/material/core';
import {StudentsModule} from "./modules/students/students.module";
import {RecordModule} from "./modules/record/record.module";
import {SubjectsModule} from "./modules/subjects/subjects.module";
import {HttpClientModule} from "@angular/common/http";
import {AgePipe} from "./utils/pipes/age.pipe";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatNativeDateModule,
    StudentsModule,
    RecordModule,
    SubjectsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
