import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Inscription, Student} from "../../../core/entities/student";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {DetailsModalComponent} from "../../students/details-modal/details-modal.component";
import {RegisterSubjectModalComponent} from "../../students/register-subject-modal/register-subject-modal.component";
import {SubjectsService} from "../../../core/services/subjects.service";

@Component({
  selector: 'app-record-home',
  templateUrl: './record-home.component.html',
  styleUrls: ['./record-home.component.css']
})
export class RecordHomeComponent implements OnInit {
  displayedColumns: string[] = ['documentNum', 'nameEst', 'typeLicense', 'nameSubj', 'module', 'state'];
  dataSource!: MatTableDataSource<Inscription>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  inscriptions: Inscription[] = [];

  constructor(private subjectsService: SubjectsService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.subjectsService.getAllRecord().subscribe(inscriptions => {
      this.inscriptions = inscriptions;
      this.dataSource = new MatTableDataSource<Inscription>(this.inscriptions);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showDetails(student: Student) {
    console.log(student);
    const dialogRef = this.matDialog.open(DetailsModalComponent, {
      width: '31rem',
      data: student,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(studentId => {
      if (studentId) {
        const dialogRef = this.matDialog.open(RegisterSubjectModalComponent, {
          width: '31rem',
          height: '20rem',
          data: student,
          disableClose: true
        });
        // dialogRef.afterClosed().subscribe(studentId => {
        //   if (studentId) {
        //
        //   }
        // });
      }
    });
  }
}
