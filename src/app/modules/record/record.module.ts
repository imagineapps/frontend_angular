import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecordHomeComponent} from './record-home/record-home.component';
import {MaterialModule} from "../../material/material.module";

@NgModule({
  declarations: [
    RecordHomeComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class RecordModule {
}
