import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterSubjectModalComponent } from './register-subject-modal.component';

describe('RegisterSubjectModalComponent', () => {
  let component: RegisterSubjectModalComponent;
  let fixture: ComponentFixture<RegisterSubjectModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterSubjectModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterSubjectModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
