import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Modules, Student} from "../../../core/entities/student";
import {AbstractControl} from "@angular/forms";
import {StudentService} from "../../../core/services/student.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SubjectsService} from "../../../core/services/subjects.service";

@Component({
  selector: 'app-register-subject-modal',
  templateUrl: './register-subject-modal.component.html',
  styleUrls: ['./register-subject-modal.component.css']
})
export class RegisterSubjectModalComponent {

  submited: boolean = false;
  modules: Modules[] = [];
  stateInscription: string = 'ACTIVE';
  idSubject: number = 0;

  constructor(public dialogRef: MatDialogRef<RegisterSubjectModalComponent>,
              private studentService: StudentService,
              private snackBar: MatSnackBar,
              private subjectsService: SubjectsService,
              @Inject(MAT_DIALOG_DATA) public student: Student) {
    this.subjectsService.getAllModules().subscribe(modules => {
      this.modules = modules;
    });
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  selectSubject(event: any) {
    this.idSubject = event;
  }

  aceptar() {
    if (this.idSubject !== 0) {
      this.subjectsService.registerInscription(this.student.id, this.idSubject, this.stateInscription).subscribe(response => {
        this.openSnackBar(response.message, 3000, 'success');
        this.dialogRef.close();
      }, error => {
        this.openSnackBar(error.error.message, 3000, 'warning');
      });
    }
    // this.studentService.registerStudent(student).subscribe(response => {
    //   this.openSnackBar(response.message, 3000, 'success');
    //   this.dialogRef.close(response.objectResponse as Student);
    // }, error => {
    //   this.openSnackBar(error.error.message, 3000, 'warning');
    // });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }
}
