import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {StudentService} from "../../../core/services/student.service";
import {Student} from "../../../core/entities/student";
import {MatDialog} from "@angular/material/dialog";
import {CreateStudentModalComponent} from "../create-student-modal/create-student-modal.component";
import {DetailsModalComponent} from "../details-modal/details-modal.component";
import {RegisterSubjectModalComponent} from "../register-subject-modal/register-subject-modal.component";

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styleUrls: ['./students-table.component.css']
})
export class StudentsTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'documentNumber', 'age', 'typeDrivingLicense'];
  dataSource!: MatTableDataSource<Student>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  students: Student[] = [];

  constructor(private studentService: StudentService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.studentService.getAllStudents().subscribe(students => {
      this.students = students;
      this.dataSource = new MatTableDataSource<Student>(this.students);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addStudent() {
    const dialogRef = this.matDialog.open(CreateStudentModalComponent, {
      width: '31rem',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(student => {
      if (student) {
        this.students.push(student);
        this.dataSource = new MatTableDataSource<Student>(this.students);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  showDetails(student: Student) {
    console.log(student);
    const dialogRef = this.matDialog.open(DetailsModalComponent, {
      width: '31rem',
      data: student,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(studentId => {
      if (studentId) {
        const dialogRef = this.matDialog.open(RegisterSubjectModalComponent, {
          width: '31rem',
          height: '20rem',
          data: student,
          disableClose: true
        });
        // dialogRef.afterClosed().subscribe(studentId => {
        //   if (studentId) {
        //
        //   }
        // });
      }
    });
  }
}
