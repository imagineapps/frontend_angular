import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Student} from "../../../core/entities/student";

@Component({
  selector: 'app-details-modal',
  templateUrl: './details-modal.component.html',
  styleUrls: ['./details-modal.component.css']
})
export class DetailsModalComponent {

  currentStudent: Student;

  constructor(public dialogRef: MatDialogRef<DetailsModalComponent>,
              @Inject(MAT_DIALOG_DATA) public student: Student) {
    this.currentStudent = student;
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  viewSubjectRegister(): void {
    this.dialogRef.close(this.currentStudent.id);
  }
}
