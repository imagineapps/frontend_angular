import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StudentsTableComponent} from './students-table/students-table.component';
import {MaterialModule} from "../../material/material.module";
import {MatTableModule} from "@angular/material/table";
import {UtilsModule} from "../../utils/utils.module";
import {CreateStudentModalComponent} from './create-student-modal/create-student-modal.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DetailsModalComponent} from './details-modal/details-modal.component';
import { RegisterSubjectModalComponent } from './register-subject-modal/register-subject-modal.component';

@NgModule({
  declarations: [
    StudentsTableComponent,
    CreateStudentModalComponent,
    DetailsModalComponent,
    RegisterSubjectModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatTableModule,
    UtilsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class StudentsModule {
}
