import {Component} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Student, TypeDrivingLicense} from "../../../core/entities/student";
import {StudentService} from "../../../core/services/student.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-create-student-modal',
  templateUrl: './create-student-modal.component.html',
  styleUrls: ['./create-student-modal.component.css']
})
export class CreateStudentModalComponent {
  studentForm: FormGroup;
  submited: boolean = false;
  minDate: Date;
  maxDate: Date;
  typeDrivingList: (string | TypeDrivingLicense)[] = Object.values(TypeDrivingLicense);

  constructor(public dialogRef: MatDialogRef<CreateStudentModalComponent>,
              private formBuilder: FormBuilder,
              private studentService: StudentService,
              private snackBar: MatSnackBar) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date();
    this.studentForm = this.formBuilder.group({
      name: ['', Validators.required],
      documentNumber: ['', Validators.required],
      birthDate: ['', Validators.required],
      typeDrivingLicense: ['', Validators.required]
    });
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar() {
    this.submited = true;
    this.studentForm.markAllAsTouched();
    if (this.studentForm.invalid) {
      return;
    }

    const student: Student = new Student();
    student.name = this.f['name'].value;
    student.documentNumber = this.f['documentNumber'].value;
    student.birthDate = this.f['birthDate'].value;
    student.typeDrivingLicense = this.f['typeDrivingLicense'].value;
    this.studentService.registerStudent(student).subscribe(response => {
      this.openSnackBar(response.message, 3000, 'success');
      this.dialogRef.close(response.objectResponse as Student);
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }

  validateRequired(control: AbstractControl) {
    return control.hasError('required') && (control.touched || this.submited);
  }

  get f() {
    return this.studentForm.controls;
  }
}
