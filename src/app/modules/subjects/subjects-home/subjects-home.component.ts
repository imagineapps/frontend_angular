import {Component} from '@angular/core';
import {SubjectsService} from "../../../core/services/subjects.service";
import {Modules} from "../../../core/entities/student";

@Component({
  selector: 'app-subjects-home',
  styleUrls: ['subjects-home.component.css'],
  templateUrl: 'subjects-home.component.html'
})
export class SubjectsHomeComponent {
  panelOpenState = false;
  modules: Modules[] = [];

  constructor(private subjectsService: SubjectsService) {
    this.subjectsService.getAllModules().subscribe(modules => {
      this.modules = modules;
    });
  }
}
