import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubjectsHomeComponent} from './subjects-home/subjects-home.component';
import {MaterialModule} from "../../material/material.module";

@NgModule({
  declarations: [
    SubjectsHomeComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class SubjectsModule {
}
