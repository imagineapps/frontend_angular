export enum TypeDrivingLicense {
  A1 = 'A1', A2 = 'A2', B1 = 'B1', B2 = 'B2', C1 = 'C1', C2 = 'C2'
}

export enum InscriptionState {
  ACTIVE = 'ACTIVE', INACTIVE = 'INACTIVE'
}

export class Modules {
  id!: number;
  name!: string;
  subjects!: Subject[];

  constructor() {
  }
}


export class Subject {
  id!: number;
  name!: string;
  modules!: Modules;
}

export class Inscription {
  id!: number;
  inscriptionDate!: string;
  inscriptionState!: InscriptionState;
  subject!: Subject;
  student!: Student;
}

export class Student {
  id!: number;
  name!: string;
  documentNumber!: string;
  birthDate!: string;
  typeDrivingLicense!: TypeDrivingLicense;
  inscriptions!: Inscription[];

  constructor() {
  }
}
