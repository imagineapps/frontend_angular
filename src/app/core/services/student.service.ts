import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Student} from "../entities/student";
import {Response} from "../entities/response";

export const ipServer = 'http://localhost:8080/api';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) {
  }

  public getAllStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(`${ipServer}/students`);
  }

  public registerStudent(student: Student): Observable<Response> {
    return this.http.post<Response>(`${ipServer}/students`, student);
  }
}
