import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ipServer} from "./student.service";
import {Inscription, Modules} from "../entities/student";
import {Response} from "../entities/response";

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {
  constructor(private http: HttpClient) {
  }

  public getAllModules(): Observable<Modules[]> {
    return this.http.get<Modules[]>(`${ipServer}/modules`);
  }

  public getAllRecord(): Observable<Inscription[]> {
    return this.http.get<Inscription[]>(`${ipServer}/inscriptions`);
  }

  public registerInscription(studentId: number, subjectId: number, state: string): Observable<Response> {
    const endpoint = ipServer + '/inscriptions?studentId=' + studentId + '&subjectId=' + subjectId + '&state=' + state;
    return this.http.post<Response>(endpoint, {});
  }
}
