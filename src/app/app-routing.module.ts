import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsTableComponent} from "./modules/students/students-table/students-table.component";
import {SubjectsHomeComponent} from "./modules/subjects/subjects-home/subjects-home.component";
import {RecordHomeComponent} from "./modules/record/record-home/record-home.component";

const routes: Routes = [
  {path: '', component: StudentsTableComponent},
  {path: 'students', component: StudentsTableComponent},
  {path: 'subjects', component: SubjectsHomeComponent},
  {path: 'record', component: RecordHomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
