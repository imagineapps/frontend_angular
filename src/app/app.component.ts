import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public opcionesMenu = [
    {name: 'ESTUDIANTES', url: '/students', icon: 'person'},
    {name: 'MODULOS Y CLASES', url: '/subjects', icon: 'view_module'},
    {name: 'REGISTRO DE CLASES', url: '/record', icon: 'restore'},
  ];

  constructor(private router: Router) {
  }

  abrir(url: string): void {
    this.router.navigate([url]);
  }
}
